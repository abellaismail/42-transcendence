'use client';

import Image from "next/image";
import Logo from "@/components/icons/logo";
import { Button } from "@/components/ui/button";
import { Bell, Search, Sword, Swords, Frown, CalendarRange, Key } from "lucide-react";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Separator } from "@/components/ui/separator";
import { Jura } from "next/font/google";
import { Dispatch, SetStateAction, useState } from "react";

const jura = Jura({ subsets: ['latin'] });

function UserAvatar({ initials, src, borders, size }: { initials: string, src: string, borders: number, size: number }) {
    return (
        <div style={{ minHeight: `${size}px`, minWidth: `${size}px` }}>
            <Avatar className="bg-on-background border border-black h-full w-full" style={{ border: `${borders}px` }}>
                <AvatarImage src={src} width={size} height={size} />
                <AvatarFallback style={{ minHeight: `${size}px`, minWidth: `${size}px` }}>{initials}</AvatarFallback>
            </Avatar>
        </div>
    );
}

type OpponentResult = {
    name: string,
    image: string,
    score: number,
};

type BattleHistory = {
    opponent1: OpponentResult,
    opponent2: OpponentResult,
    date: string
};

function* range(start: number, end: number) {
    for (let i = start; i < end; i++) {
        yield i;
    }
}

function map<InType, InReturn, OutType, OutReturn>(
    source: Generator<InType, InReturn, unknown>,
    mapper: (item: InType) => Generator<OutType, OutReturn, unknown>
): Array<OutType> {

    const mappedItems: Array<OutType> = [];

    for (const item of source) {
        for (const mapped of mapper(item)) {
            mappedItems.push(mapped);
        }
    }

    return mappedItems;
}

function MatchHistory({ opponent1, opponent2, date }: BattleHistory) {
    return (
        <div className="flex justify-between items-center gap-[40px]">
            <div className="flex flex-1 items-center justify-center gap-[30px] text-[16px] text-on-background">
                <UserAvatar initials="SO" borders={1} size={30} src={opponent1.image} />
                <p className="font-semibold text-ellipsis overflow-hidden line-clamp-1">{opponent1.name}</p>
                <p className={`${jura.className}`}>{opponent1.score}</p>
                <Swords size={15} />
                <p className={`${jura.className}`}>{opponent2.score}</p>
                <p className="font-semibold text-ellipsis overflow-hidden line-clamp-1">{opponent2.name}</p>
                <UserAvatar initials="SO" borders={1} size={30} src={opponent2.image} />
            </div>
            <div className="flex items-center gap-[8px]">
                <CalendarRange className="stroke-primary" size={14} />
                <p className="text-[14px] text-primary">{date}</p>
            </div>
        </div>
    );
}

function stateOf<T>(value: T | (() => T)): { get: T, set: Dispatch<SetStateAction<T>> } {
    const [get, set] = useState(value);
    return { get, set };
}

export default function Profile() {

    const showAchivements = stateOf(false);
    const showFriends = stateOf(false);

    return (
        <div className="flex flex-col container mx-auto bg-background gap-[30px] py-[30px] md:py-[49px]">
            <div className="flex w-full justify-between">
                <div className="flex items-center gap-[22px]">
                    <Logo className="min-w-[57px] min-h-[57px] fill-on-background" width={57} height={57} />
                    <p className="hidden md:block font-extrabold">
                        <span className="text-[40px] text-primary">Ping </span>
                        <span className="text-[30px] text-on-background">Pong</span>
                    </p>
                </div>
                <div className="flex items-center gap-[35px]">
                    <Button className="hidden lg:flex text-on-background justify-start text-[16px] w-[276px] shrink" variant="outline">
                        Search
                    </Button>
                    <Search className="lg:hidden stroke-on-background" />
                    <Bell className="min-w-[24px] min-h-[24px] stroke-on-background" width={24} height={24} />
                    <UserAvatar initials="SO" src="https://placehold.co/40" borders={1} size={40} />
                </div>
            </div>
            <div className="rounded-[10px] bg-primary flex-1 max-h-[180px] md:max-h-fit md:px-[42px] md:py-[38px] md:gap-[32px]">
                <div className="flex flex-col md:flex-row items-center md:items-end translate-y-[80px] md:translate-y-0 md:gap-[30px]">
                    <UserAvatar initials="SO" src="https://placehold.co/140" borders={3} size={140} />
                    <div className="flex flex-col mt-[16px] md:pt-0 md:justify-end text-on-background md:text-on-primary text-center md:text-start">
                        <p className="overflow-hidden line-clamp-1 text-ellipsis font-semibold text-[20px] md:text-[28px]">
                            Placeholder Name
                        </p>
                        <p className="overflow-hidden line-clamp-1 text-ellipsis font-light text-[14px] pt-[8px] md:pt-0 md:text-[18px]">
                            A possibly very very very very very very long status of the user
                        </p>
                    </div>
                    <div className="flex md:flex-1 justify-center mt-[16px] md:pt-0 text-on-background gap-[16px] md:text-on-primary">
                        <div className="flex items-center gap-[8px]">
                            <Swords className="min-w-[24px] min-h-[24px] stroke-on-background md:stroke-on-primary" />
                            <p className="text-[18px] text-on-background md:text-on-primary line-clamp-1">10K Battles</p>
                        </div>
                        <div className="flex items-center gap-[8px]" >
                            <Sword className="min-w-[24px] min-h-[24px] stroke-on-background md:stroke-on-primary" />
                            <p className="text-[18px] text-on-background md:text-on-primary line-clamp-1">9K Wins</p>
                        </div>
                        <div className="flex items-center gap-[8px]">
                            <Frown className="min-w-[24px] min-h-[24px] stroke-on-background md:stroke-on-primary" />
                            <p className="text-[18px] text-on-background md:text-on-primary line-clamp-1">1K Lose</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex flex-col gap-[30px] translate-y-[155px] md:translate-y-0">
                <Button className="bg-on-background gap-[8px] md:hidden">Ask For Battle <Swords width={16} height={16} /></Button>
                <div className="grid grid-cols-10 gap-[30px]">
                    <div className="flex col-span-full xl:col-span-3 flex-col gap-[30px]">
                        <div>
                            <div onClick={() => showAchivements.set(!showAchivements.get)} className="flex gap-[30px] items-center justify-between p-[25px] border-primary border rounded-[6px] mb-[10px]">
                                <p className="font-semibold text-[24px] text-on-background">Achievements</p>
                                <p className="font-medium text-[16px] text-accent">View All</p>
                            </div>
                            {
                                showAchivements.get && <div className="grid grid-cols-[repeat(auto-fit,minmax(100px,1fr))] justify-center p-[25px] border-gray-on-backgorund border rounded-[6px] gap-[30px]">
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_0.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_1.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_2.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_3.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_4.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_5.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_6.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_7.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_8.png" alt="achievement" width={80} height={80} />
                                    <Image className="h-auto mx-auto" src="/achievements/achievement_9.png" alt="achievement" width={80} height={80} />
                                </div>
                            }
                        </div>
                        <div>
                            <div onClick={() => showFriends.set(!showFriends.get)} className="flex gap-[30px] items-center justify-between p-[25px] border-primary border rounded-[6px] mb-[10px]">
                                <p className="font-semibold text-[24px] text-on-background">Friends</p>
                                <p className="font-medium text-[16px] text-accent">View All</p>
                            </div>
                            {
                                showFriends.get && <div className="flex justify-between items-center p-[25px] border-gray-on-backgorund border rounded-[6px] gap-[30px]">
                                    <div className="flex -space-x-5">
                                        <UserAvatar borders={1} size={40} initials="SO" src="https://placehold.co/40" />
                                        <UserAvatar borders={1} size={40} initials="SO" src="https://placehold.co/40" />
                                        <UserAvatar borders={1} size={40} initials="SO" src="https://placehold.co/40" />
                                        <UserAvatar borders={1} size={40} initials="SO" src="https://placehold.co/40" />
                                        <UserAvatar borders={1} size={40} initials="SO" src="https://placehold.co/40" />
                                    </div>
                                    <p className="text-on-background text-[16px] ">300 Friends</p>
                                </div>
                            }
                        </div>
                    </div>
                    <div className="col-span-full xl:col-span-7">
                        <div className="flex gap-[30px] items-center justify-between p-[25px] border-primary border rounded-[6px] mb-[10px]">
                            <p className="font-semibold text-[24px] text-on-background">Battle History</p>
                            <Button className="bg-on-background gap-[8px] hidden md:flex">Ask For Battle <Swords width={16} height={16} /></Button>
                        </div>
                        <div className="flex flex-col p-[25px] border-gray-on-backgorund border rounded-[6px] gap-[30px]">
                            {
                                map(range(0, 10), index => function* () {
                                    if (index > 0) {
                                        yield <Separator className="w-[80%]" key={index + '0'} />
                                    }
                                    yield <MatchHistory
                                        opponent1={{ name: "Placeholder name", image: "https://placehold.co/40", score: 10 }}
                                        opponent2={{ name: "Placeholder name", image: "https://placehold.co/40", score: 10 }}
                                        date="10 minutes ago"
                                        key={index}
                                    />
                                }())
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
