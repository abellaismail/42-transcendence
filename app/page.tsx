import { PlayCircle, BarChart2, TrendingUp, Menu, ArrowLeft } from 'lucide-react';
import { Sheet, SheetTrigger, SheetContent, SheetClose } from '@/components/ui/sheet'
import { Handlee } from 'next/font/google';
import { Button } from '@/components/ui/button';
import FortyTwo from '@/components/icons/forty-two';
import Logo from '@/components/icons/logo';
import Link from 'next/link';

const handlee = Handlee({ subsets: ['latin'], weight: '400' });

export default function Home() {
  return (
    <div className="flex flex-col bg-landing-background bg-cover bg-no-repeat w-[100dvw] min-h-[100dvh] px-[30px] gap-[36px]">
      <div className="flex items-center justify-between w-full pt-[30px] md:pt-[49px] md:px-[98px]">
        <Logo className="w-[57px] h-[57px] fill-on-background" />
        <Button className="hidden md:flex text-on-background bg-transparent fill-white hover:fill-black" variant="outline">
          <FortyTwo className="w-[16px] h-[16px] me-[8px]" />
          Login with your account
        </Button>
        <div className="md:hidden">
          <Sheet>
            <SheetTrigger>
              <Menu className='w-[44px] h-[44px] stroke-white' width={44} height={44} />
            </SheetTrigger>
            <SheetContent className='min-w-full min-h-full p-0 m-0 border-0'>
              <div className='flex flex-col h-full'>
                <div className='w-full py-[30px] px-[30px]'>
                  <SheetClose>
                    <ArrowLeft className="w-[44px] h-[44px] dark: stroke-white" width={44} height={44} />
                  </SheetClose>
                </div>
                <div className='flex flex-col flex-1 w-full px-[30px] pb-[70px] pt-[50px]'>
                  <div className='flex flex-col flex-1 items-center'>
                    <p className='text-primary font-bold text-[30px] mb-[18px]'>Login</p>
                    <p className='text-on-background font-light text-[16px]'>Log in with your 42 Account</p>
                  </div>
                  <div className='flex flex-col flex-none items-center'>
                    <Button className='mt-[36px] py-[25px] px-[24px] mx-auto mb-[80px]'>
                      <FortyTwo className='me-[10px] w-[24px] h-[24px] fill-white' />
                      <span className='font-bold text-on-primary text-[16px]'>Login with 42</span>
                    </Button>
                    <p className='text-on-background text-center font-light'>
                      By clicking continue, you agree to our{" "}
                      <Link className='text-primary underline' href="#">Terms of Service</Link>
                      {" and "}
                      <Link className='text-primary underline' href="#">Privacy Policy</Link>.
                    </p>
                  </div>
                </div>
              </div>
            </SheetContent>
          </Sheet>
        </div>
      </div>
      <div className='flex flex-col flex-grow justify-center'>
        <div className='flex flex-col text-center text-on-background'>
          <span className='font-extrabold'>
            <span className='text-[36px] md:text-[100px]'>Ping</span>{" "}
            <span className='text-[27px] md:text-[76px] text-primary'>Pong</span>
          </span>
          <span className={`${handlee.className} mt-[20px] text-[32px] md:text-[54px]`}>The game you missed from the 90&apos;s</span>
        </div>
        <Button className='mt-[36px] py-[25px] px-[24px] mx-auto'>
          <PlayCircle className='me-[10px] w-[24px] h-[24px] stroke-on-primary' width={24} height={24} />
          <span className='font-bold text-on-primary text-[16px]'>Getting Started</span>
        </Button>
      </div>
      <div className='flex flex-col md:flex-row items-center justify-center pb-[36px] gap-[20px] md:gap-[42px]'>
        <div className='flex gap-[10px]'> 
          <BarChart2 className='stroke-primary' />
          <p className='text-[16px] font-light'>
            <span className='text-on-background'>Match Played: </span>
            <span className='text-primary'>+9323</span>
          </p>
        </div>
        <span className='text-primary text-[8px] hidden md:block'>●</span>
        <div className='flex gap-[10px]'>
          <TrendingUp className='stroke-primary' />
          <p className='text-[16px] font-light'>
            <span className='text-primary'>+9.2% </span>
            <span className='text-on-background'>Than Last Month</span>
          </p>
        </div>
      </div>
    </div>
  );
}
